const Cc = Components.classes;
const Ci = Components.interfaces;
const Cu = Components.utils;

Cu.import("resource://gre/modules/Services.jsm");

// orientation
var lastOrientation = null;

// toolbar icon id
var pageactionId=null;

// toolbar icon options 
var paOptions;

// Is this the android firefox UI ?
function isNativeUI() {
  return (Services.appinfo.ID == "{aa3c5121-dab2-40e2-81ca-7ea25febc110}");
}

// Alert a message
function showMsg(win,msg) {
  win.NativeWindow.toast.show(msg, "short");
}

// Called when orientation changes. 
function orientationChanged(win,orientation){
	paOptions = {
	  title: "orientation",
	  icon: "",
	  clickCallback: function() { showMsg(win,"single-click"); },
	  longClickCallback: function() { showMsg(win,"long-click"); }
	};
	
	if(orientation === "portrait-secondary" || orientation === "portrait-primary"){
		//device is in portrait
		paOptions.icon = "chrome://orientation/skin/portrait.png";
	}else{
		//device is landscape
		paOptions.icon = "chrome://orientation/skin/landscape.png";
	}
	
	if(pageactionId){
		win.NativeWindow.pageactions.remove(pageactionId);
	}	
	
	if(win.NativeWindow.pageactions){ 
		pageactionId = win.NativeWindow.pageactions.add(paOptions);
	}
	showMsg(win,orientation);
	
}

//Initialize listener to a window
function loadIntoWindow(win) {
	if (!win)
		return;
		
	if (isNativeUI()) {
		//Makes sure orientation check is only done on firefox android ver.
		//Add a listner even for orientation change. Make it only trigger
		//popup when orientation has changed.
		win.ondeviceorientation = function(){
			var orientation =  win.screen.mozOrientation;
			if(lastOrientation!=orientation){
				orientationChanged(win,orientation);
				lastOrientation = orientation;
			}
		};
		
	}
}

//Free listeners on the window
function unloadFromWindow(win) {
	if (!win)
		return;
  
	if (isNativeUI()) {
		win.NativeWindow.pageactions.remove(pageactionId);
	}
}


/**
 * bootstrap.js API
 */
var windowListener = {
  onOpenWindow: function(aWindow) {
    // Wait for the window to finish loading
    let domWindow = aWindow.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindowInternal || Ci.nsIDOMWindow);
    domWindow.addEventListener("load", function() {
      domWindow.removeEventListener("load", arguments.callee, false);
      loadIntoWindow(domWindow);
    }, false);
  },
  
  onCloseWindow: function(aWindow) {
  },
  
  onWindowTitleChange: function(aWindow, aTitle) {
  }
};

function startup(aData, aReason) {
  // Load into any existing windows
  let windows = Services.wm.getEnumerator("navigator:browser");
  while (windows.hasMoreElements()) {
    let domWindow = windows.getNext().QueryInterface(Ci.nsIDOMWindow);
    loadIntoWindow(domWindow);
  }

  // Load into any new windows
  Services.wm.addListener(windowListener);
}

function shutdown(aData, aReason) {
  // When the application is shutting down we normally don't have to clean
  // up any UI changes made
  if (aReason == APP_SHUTDOWN)
    return;

  // Stop listening for new windows
  Services.wm.removeListener(windowListener);

  // Unload from any existing windows
  let windows = Services.wm.getEnumerator("navigator:browser");
  while (windows.hasMoreElements()) {
    let domWindow = windows.getNext().QueryInterface(Ci.nsIDOMWindow);
    unloadFromWindow(domWindow);
  }
}

function install(aData, aReason) {
}

function uninstall(aData, aReason) {
}
