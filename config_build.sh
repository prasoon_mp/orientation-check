#!/bin/bash

# Build config for the build script, build.sh. Look there for more info.

APP_NAME=orientation
CHROME_PROVIDERS="skin"
CLEAN_UP=1
ROOT_FILES="bootstrap.js"
ROOT_DIRS=
BEFORE_BUILD=
AFTER_BUILD=
PUSH_TO_DEVICE=0
ANDROID_APP_ID=org.mozilla.firefox
